stages:
  - setup
  - tests
  - build
  - deploy

image: composer

variables: 
  ARTIFACT_NAME: bundle_v$CI_PIPELINE_IID.zip
  APP_NAME: laravel-test
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task

setup:
  stage: setup
  script:
    - composer update --no-cache
    - composer install 
    - cp .env.example .env
    - php artisan key:generate
  artifacts:
    paths:
      - ./

unit tests:
  stage: tests
  script:
    - vendor/bin/phpunit --log-junit report.xml
  artifacts:
    when: always
    reports:
      junit: report.xml

sonarcloud-check:
  stage: tests
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  only:
    - merge_requests
    - master
    - develop

build bundle:
  stage: build
  image: alpine
  before_script:
    - apk add zip
    - mkdir build
  script:
    - zip -r ./build/$ARTIFACT_NAME .
    - ls -l ./build | grep $ARTIFACT_NAME
  dependencies: []
  artifacts:
    name: $ARTIFACT_NAME
    paths: 
      - ./
    exclude:
      - .git/**/*
      - .gitlab-ci.yml
      - .gitignore
      - .gitattributes
      - .env.example
      - sonar-project.properties
      - composer.lock
      - README.md
      - .editorconfig

deploy:
  stage: deploy
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  before_script:
    - yum install -y curl
    - yum install -y jq
  script:
    - aws --version
    - aws configure set region eu-west-3
    - aws s3 cp ./build/$ARTIFACT_NAME s3://$S3_BUCKET/$ARTIFACT_NAME
    - aws elasticbeanstalk create-application-version --application-name $APP_NAME --version-label $CI_PIPELINE_IID --source-bundle S3Bucket=$S3_BUCKET,S3Key=$ARTIFACT_NAME
    - CNAME=$(aws elasticbeanstalk update-environment --application-name $APP_NAME --environment-name "Laraveltest-staging" --version-label=$CI_PIPELINE_IID --option-settings ResourceName="version",Namespace="aws:elasticbeanstalk:application:environment",OptionName="APP_VERSION",Value="$CI_PIPELINE_IID" | jq '.CNAME' --raw-output)
    - echo $CNAME
    - sleep 45
    - curl http://$CNAME/ | grep "Laravel"
