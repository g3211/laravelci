<?php

namespace App\Http\Interfaces;

interface Test
{
    public function thisIsAFunction(): void;
}
